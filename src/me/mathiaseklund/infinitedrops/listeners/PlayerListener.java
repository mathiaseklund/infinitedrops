package me.mathiaseklund.infinitedrops.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.infinitedrops.Main;
import me.mathiaseklund.infinitedrops.objects.DropsInventory;
import me.mathiaseklund.infinitedrops.utility.Global;

public class PlayerListener implements Listener {

	Main main = Main.getMain();

	/**
	 * Creates a new DropsInventory for new players.
	 */
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		new DropsInventory(player);
	}

	/**
	 * Removes the players DropsInventory from the server.
	 */
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		Global.DropsInventories.get(player.getUniqueId().toString()).SaveData();
		Global.DropsInventories.remove(player.getUniqueId().toString());
	}

	/**
	 * Handles Inventory Clicks for the InfiniteDrops inventory.
	 */
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (!event.isCancelled()) {
			String title = event.getInventory().getTitle();

			if (title.equalsIgnoreCase(Global.DropsInventoryTemplate.getTitle())) {

				if (event.getCurrentItem() != null) {
					if (event.getCurrentItem().getType() != Material.AIR) {

						ItemStack ci = event.getCurrentItem();

						if (ci.hasItemMeta()) {

							ItemMeta cim = ci.getItemMeta();
							if (cim.hasDisplayName()) {
								if (cim.getDisplayName().equalsIgnoreCase("Sell All")) {
									// TODO Clicked on Sell All item
									Player player = (Player) event.getWhoClicked();
									Global.DropsInventories.get(player.getUniqueId().toString()).SellAll();
								} else if (cim.getDisplayName().equalsIgnoreCase("Sell Bonus")) {
									// TODO Clicked on Sell Bonus item
								} else if (cim.getDisplayName().equalsIgnoreCase(" ")) {
									// TODO Clicked on Border item
								}
							} else {
								// CurrentItem does not have a display name
								Player player = (Player) event.getWhoClicked();
								if (event.getClick() == ClickType.RIGHT) {
									// Right clicked an item. Move a stack to your inventory.
									Global.DropsInventories.get(player.getUniqueId().toString())
											.RetrieveDrop(ci.getTypeId());
								} else {
									List<String> cil = cim.getLore();
									double value = 0;
									for (String s : cil) {
										if (s.contains("Value: ")) {
											String vS = s.split(" ")[1];
											vS = vS.replaceAll(",", "");
											double v = Double.parseDouble(vS);
											value = v;
										}
									}
									Global.DropsInventories.get(player.getUniqueId().toString())
											.SellDrop(ci.getTypeId(), value);
								}
							}
						} else {
							// CurrentItem does not have any item meta. INVALID ITEM
						}
					} else {
						// Player clicked AIR
					}
				} else {
					// Player clicked outside inventory
				}
				event.setCancelled(true);
			}
		}
	}

	/**
	 * Handles the moving of the item into the InfiniteDrops inventory.
	 */
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		if (event.getEntity().getKiller() instanceof Player) {
			Player killer = event.getEntity().getKiller();
			List<ItemStack> drops = event.getDrops();
			List<ItemStack> newDrops = new ArrayList<ItemStack>();

			/**
			 * Check if Item is whitelisted to be sent to the DropsInventory.
			 */
			for (ItemStack is : drops) {
				@SuppressWarnings("deprecation")
				int id = is.getTypeId();
				if (main.getConfig().get("drops." + id) != null) {
					double value = main.getConfig().getDouble("drops." + id);
					DropsInventory di = Global.DropsInventories.get(killer.getUniqueId().toString());
					di.AddDrop(id, is.getAmount(), value);
				} else {
					// Dropped item will not be moved to DropsInventory
					newDrops.add(is);
				}
			}
			event.getDrops().clear();
			for (ItemStack is : newDrops) {
				event.getEntity().getWorld().dropItem(event.getEntity().getLocation(), is);
			}
		}
	}

	/**
	 * Handle sending dropped blocks into the DropsInventory
	 */
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if (!event.isCancelled()) { // Make sure event is not cancelled by something else.
			Player player = event.getPlayer();
			if (player.getGameMode() == GameMode.SURVIVAL) { // Make sure the player is in survival mode.
				Collection<ItemStack> newDrops = event.getBlock().getDrops();
				newDrops.clear();

				/**
				 * Check if Item is whitelisted to be sent to the DropsInventory.
				 */
				for (ItemStack is : event.getBlock().getDrops()) {
					@SuppressWarnings("deprecation")
					int id = is.getTypeId();
					if (main.getConfig().get("drops." + id) != null) {
						double value = main.getConfig().getDouble("drops." + id);
						DropsInventory di = Global.DropsInventories.get(player.getUniqueId().toString());
						di.AddDrop(id, is.getAmount(), value);
					} else {
						newDrops.add(is);
					}

				}

				/**
				 * Cancel the event and remove the block from the world and spawn the leftover
				 * drops.
				 */
				event.setCancelled(true);
				event.getBlock().setType(Material.AIR);
				for (ItemStack is : newDrops) {
					event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), is);
				}
			}
		}
	}
}

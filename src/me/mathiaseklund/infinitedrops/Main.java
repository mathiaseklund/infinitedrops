package me.mathiaseklund.infinitedrops;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.infinitedrops.commands.CommandDrops;
import me.mathiaseklund.infinitedrops.listeners.PlayerListener;
import me.mathiaseklund.infinitedrops.objects.DropsInventory;
import me.mathiaseklund.infinitedrops.utility.Global;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin {

	static Main main;
	File dataFile;
	public FileConfiguration data;
	public static Economy economy = null;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		System.out.println("[START] Enabling InfiniteDrops!");
		main = this;

		if (!setupEconomy()) {
			System.out.println(
					String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		LoadCommands();
		RegisterEvents();
		LoadYMLs();
		Global.SetupInventoryTemplate();
		for (Player p : Bukkit.getOnlinePlayers()) {
			new DropsInventory(p);
		}
	}

	public void onDisable() {
		System.out.println("[EXIT] Disabling InfiniteDrops!");
		for (Player p : Bukkit.getOnlinePlayers()) {
			Global.DropsInventories.get(p.getUniqueId().toString()).SaveData();
		}
	}

	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager()
				.getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}

	public void RegisterEvents() {
		this.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
	}

	/**
	 * Load all default YML files
	 */
	public void LoadYMLs() {
		this.saveDefaultConfig();
		dataFile = new File(getDataFolder(), "data.yml");
		data = YamlConfiguration.loadConfiguration(dataFile);
		if (!dataFile.exists()) {
			data.options().copyDefaults(true);
			saveData();
		}
	}

	/**
	 * Save player data file
	 */
	public void saveData() {
		try {
			data.save(dataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void LoadCommands() {
		this.getCommand("drops").setExecutor(new CommandDrops());
	}
}

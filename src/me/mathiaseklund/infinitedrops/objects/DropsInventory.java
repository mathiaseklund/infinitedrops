package me.mathiaseklund.infinitedrops.objects;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.infinitedrops.Main;
import me.mathiaseklund.infinitedrops.utility.Global;
import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.economy.EconomyResponse;

public class DropsInventory {

	ArrayList<String> content = new ArrayList<String>();

	Player owner;
	double multiplier;

	public DropsInventory(Player owner) {
		this.owner = owner;
		multiplier = 1.0;
		if (!Global.DropsInventories.containsKey(owner.getUniqueId().toString())) {
			Global.DropsInventories.put(owner.getUniqueId().toString(), this);
			LoadData();
		}
	}

	public void LoadData() {
		Main main = Main.getMain();
		if (main.data.get(owner.getUniqueId().toString()) != null) {
			content.addAll(main.data.getStringList(owner.getUniqueId().toString() + ".content"));
			main.data.set(owner.getUniqueId().toString(), null);
			main.saveData();
		} else {
			// No prior data found
		}
	}

	public void SaveData() {
		Main main = Main.getMain();
		main.data.set(owner.getUniqueId().toString() + ".content", content);
		main.saveData();
	}

	/**
	 * Open the drops inventory.
	 */
	public void OpenInventory() {
		Main main = Main.getMain();
		ItemStack[] content = Global.DropsInventoryTemplate.getContents();
		Inventory inv = Bukkit.createInventory(owner, 54, Global.DropsInventoryTemplate.getTitle());
		inv.setContents(content);

		// Get Drops
		for (String s : this.content) {
			int id = Integer.parseInt(s.split(" ")[0]);
			int amount = Integer.parseInt(s.split(" ")[1]);
			double value = Double.parseDouble(s.split(" ")[2]);

			@SuppressWarnings("deprecation")
			ItemStack is = new ItemStack(id);
			ItemMeta im = is.getItemMeta();
			// TODO Get lore from Config
			ArrayList<String> lore = new ArrayList<String>();
			for (String str : main.getConfig().getStringList("gui.dropsinventory.drop.lore")) {
				str = str.replace("{value}", value + "");
				str = str.replace("{amount}", amount + "");
				lore.add(ChatColor.translateAlternateColorCodes('&', str));
			}
			im.setLore(lore);
			is.setItemMeta(im);
			inv.addItem(is);
		}

		owner.openInventory(inv);

	}

	/**
	 * Add a dropped item into the DropsInventory content list.
	 * 
	 * @param itemId
	 *            - ID if item that was dropped.
	 * @param itemAmount
	 *            - Amount of said item that was dropped.
	 * @param itemValue
	 *            - Value of said item that was dropped.
	 */
	public void AddDrop(int itemId, int itemAmount, double itemValue) {
		ArrayList<String> newContent = new ArrayList<String>();
		boolean updated = false;
		for (String s : content) {
			int id = Integer.parseInt(s.split(" ")[0]);
			int amount = Integer.parseInt(s.split(" ")[1]);
			double value = Double.parseDouble(s.split(" ")[2]);
			if (id == itemId) {
				amount += itemAmount;
				value += itemValue;
				updated = true;
			}
			String itemString = id + " " + amount + " " + value;
			newContent.add(itemString);
		}
		if (!updated) {
			String itemString = itemId + " " + itemAmount + " " + itemValue;
			newContent.add(itemString);
		}
		content = newContent;
	}

	/**
	 * Sell a stack of drops.
	 * 
	 * @param itemId
	 *            - ID of items to be sold.
	 * @param value
	 *            - Value of items to be sold
	 */
	public void SellDrop(int itemId, double value) {
		ArrayList<String> newContent = new ArrayList<String>();
		for (String s : content) {
			int id = Integer.parseInt(s.split(" ")[0]);
			if (id == itemId) {
				value = Double.parseDouble(s.split(" ")[2]);
			} else {
				newContent.add(s);
			}
		}
		if (VaultTransaction(value)) {
			String message = ChatColor.translateAlternateColorCodes('&',
					Main.getMain().getConfig().getString("messages.drops.sold"));
			message = message.replace("{value}", value + "");
			owner.sendMessage(message);
			this.content = newContent;
		}
		OpenInventory();
	}

	/**
	 * Sell all drops
	 */
	public void SellAll() {
		double value = 0;
		int totalAmount = 0;
		for (String s : content) {
			int amount = Integer.parseInt(s.split(" ")[1]);
			double v = Double.parseDouble(s.split(" ")[2]);
			value += v;
			totalAmount += amount;
		}
		if (value > 0) {
			if (VaultTransaction(value)) {
				String message = ChatColor.translateAlternateColorCodes('&',
						Main.getMain().getConfig().getString("messages.drops.sold_all"));
				message = message.replace("{value}", value + "");
				message = message.replace("{amount}", totalAmount + "");
				owner.sendMessage(message);
				this.content = new ArrayList<String>();
			}
			OpenInventory();
		}
	}

	/**
	 * Perform a Deposit transaction with vault and get the result.
	 * 
	 * @param value
	 *            - the amount of money to ne deposited to account.
	 * @return true = transaction success, false = failed transaction
	 */
	public boolean VaultTransaction(double value) {
		Main.getMain();
		EconomyResponse r = Main.economy.depositPlayer(owner, value);
		if (r.transactionSuccess()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Retrieve a stack of the selected item from your DropsInventory
	 * 
	 * @param typeId
	 *            - Item ID of item you want to retrieve.
	 */
	@SuppressWarnings("deprecation")
	public void RetrieveDrop(int typeId) {
		double singleValue = Main.getMain().getConfig().getDouble("drops." + typeId);
		ArrayList<String> newContent = new ArrayList<String>();
		for (String s : content) {
			int id = Integer.parseInt(s.split(" ")[0]);
			if (id == typeId) {
				int amount = Integer.parseInt(s.split(" ")[1]);
				double value = Double.parseDouble(s.split(" ")[2]);
				if (amount > 64) {
					// More than 1 stack.
					amount = amount - 64;
					value = value - (singleValue * 64);
					String itemString = id + " " + amount + " " + value;
					newContent.add(itemString);
					ItemStack is = new ItemStack(Material.getMaterial(id), 64);
					owner.getInventory().addItem(is);
				} else {
					// Less than/equal to 64 items
					ItemStack is = new ItemStack(Material.getMaterial(id), amount);
					owner.getInventory().addItem(is);
				}
			} else {
				newContent.add(s);
			}
		}
		this.content = newContent;
		OpenInventory();
	}
}

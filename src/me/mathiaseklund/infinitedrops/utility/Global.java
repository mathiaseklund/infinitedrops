package me.mathiaseklund.infinitedrops.utility;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.infinitedrops.Main;
import me.mathiaseklund.infinitedrops.objects.DropsInventory;

public class Global {

	public static Inventory DropsInventoryTemplate;
	public static HashMap<String, DropsInventory> DropsInventories = new HashMap<String, DropsInventory>();

	/**
	 * Setup the default InfiniteDrops Inventory template. (Border etc.)
	 * 
	 * Use this at plugin startup.
	 */
	public static void SetupInventoryTemplate() {
		DropsInventoryTemplate = Bukkit.createInventory(null, 54, "Infinite Drops"); // TODO Get name from config
		Main main = Main.getMain();
		ItemStack b = new ItemStack(Material.STAINED_GLASS_PANE); // TODO Get item from config
		ItemMeta bm = b.getItemMeta();
		bm.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				main.getConfig().getString("gui.dropsinventory.border.displayname"))); // TODO Get name from config
		b.setItemMeta(bm);

		ItemStack sa = new ItemStack(Material.EMERALD); // TODO Get item from config
		ItemMeta sam = sa.getItemMeta();
		sam.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				main.getConfig().getString("gui.dropsinventory.sell_all.displayname"))); // TODO Get name from config
		ArrayList<String> sal = new ArrayList<String>();
		for (String s : main.getConfig().getStringList("gui.dropsinventory.sell_all.lore")) {
			sal.add(ChatColor.translateAlternateColorCodes('&', s));
		}
		sam.setLore(sal);
		sa.setItemMeta(sam);

		@SuppressWarnings("deprecation")
		ItemStack sb = new ItemStack(Material.getMaterial(347)); // TODO Get item from config
		ItemMeta sbm = sb.getItemMeta();
		sbm.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				main.getConfig().getString("gui.dropsinventory.sell_bonus.displayname"))); // TODO Get name from config
		ArrayList<String> sbl = new ArrayList<String>();
		for (String s : main.getConfig().getStringList("gui.dropsinventory.sell_bonus.lore")) {
			sbl.add(ChatColor.translateAlternateColorCodes('&', s));
		}
		sbm.setLore(sbl);
		sb.setItemMeta(sbm);

		// Row #1
		DropsInventoryTemplate.setItem(0, b);
		DropsInventoryTemplate.setItem(1, b);
		DropsInventoryTemplate.setItem(2, b);
		DropsInventoryTemplate.setItem(3, b);
		DropsInventoryTemplate.setItem(4, b);
		DropsInventoryTemplate.setItem(5, b);
		DropsInventoryTemplate.setItem(6, b);
		DropsInventoryTemplate.setItem(7, b);
		DropsInventoryTemplate.setItem(8, b);

		// Row #2
		DropsInventoryTemplate.setItem(9, b);
		DropsInventoryTemplate.setItem(17, b);

		// Row #3
		DropsInventoryTemplate.setItem(18, b);
		DropsInventoryTemplate.setItem(26, b);

		// Row #4
		DropsInventoryTemplate.setItem(27, b);
		DropsInventoryTemplate.setItem(35, b);

		// Row #5
		DropsInventoryTemplate.setItem(36, b);
		DropsInventoryTemplate.setItem(44, b);

		// Row #6
		DropsInventoryTemplate.setItem(45, b);
		DropsInventoryTemplate.setItem(46, b);
		DropsInventoryTemplate.setItem(47, b);

		DropsInventoryTemplate.setItem(48, sb);

		DropsInventoryTemplate.setItem(49, b);

		DropsInventoryTemplate.setItem(50, sa);

		DropsInventoryTemplate.setItem(51, b);
		DropsInventoryTemplate.setItem(52, b);
		DropsInventoryTemplate.setItem(53, b);
	}
}
